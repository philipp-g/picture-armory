import os
import uuid
from typing import AnyStr, Tuple

# write files to a specific folder at a base path
class FileWriter:
    def __init__(self, target_base_path: AnyStr, folder: AnyStr):
        self.target_base_path = target_base_path
        self._target_folder = folder

    def write_tuple(self, data_tuple: Tuple[bytes, ...], filename):
        target_path = self.target_base_path + "/" + self._target_folder
        self._mk_target_folder(target_path)
        with open(target_path + "/" + filename, "wb") as f:
            for entry in data_tuple:
                f.write(entry)

    @property
    def target_folder(self):
        return self._target_folder

    @target_folder.setter
    def target_folder(self, value):
        self._target_folder = value

    @staticmethod
    def _mk_target_folder(target_path):
        if not os.path.isdir(target_path):
            os.mkdir(target_path)


# write files to random folder names (uuid4)
class RandomFolderFileWriter(FileWriter):
    def __init__(self, target_base_path: AnyStr):
        super().__init__(target_base_path, self.random_folder_name())

    @staticmethod
    def random_folder_name() -> AnyStr:
        return str(uuid.uuid4())

    def write_tuple(self, data_tuple: Tuple[bytes, ...], filename):
        self.target_folder = self.random_folder_name()
        super().write_tuple(data_tuple, filename)


# don't write anything
class DryFileWriter(FileWriter):

    def __init__(self):
        super().__init__("target_base_path", "folder")

    def write_tuple(self, data_tuple: Tuple[bytes, ...], filename):
        pass
