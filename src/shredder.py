import logging
from abc import ABC, abstractmethod
from typing import AnyStr

# sh uses custom import hook for magic importing of any shell command. IDE just can't discover it.
from sh import shred, rm

logger = logging.getLogger('root')


class AbstractShredder(ABC):

    @abstractmethod
    def shred(self, path: AnyStr):
        pass


class FileRemover(AbstractShredder):

    def shred(self, path: AnyStr):
        rm(path)


class FileShredder(AbstractShredder):

    def shred(self, path: AnyStr):
        shred('--remove', path)


class DryShredder(AbstractShredder):
    def shred(self, path: AnyStr):
        logger.debug("DryShredder: NOT removing file %s", path)
