#!/usr/bin/python3
import glob
import os
import signal
import sys
import time
from functools import partial
from typing import Iterator, List

import pyudev
from pyudev import Device, Monitor
from sh import sudo, ErrorReturnCode_32, mkdir

import led_control as led
import log
from encryptor import PyNaClEncryptor
from file_writer import FileWriter

# dir to mount usb media to
MOUNT_DIR = '/media/usb'
PTP_MOUNT_DIR = '/home/usbarmory/camera'
# file extensions which should be encrypted
FILE_EXTENSIONS = (".jpeg", ".jpg", ".JPEG", ".JPG")

logger = log.setup_logger('root')
# encryption strategy to use
encryptor = PyNaClEncryptor("public_pynacl.pem", FileWriter('/home/usbarmory/', 'pictures'), "private_pynacl.pem")


def signal_handler(sig, frame):
    logger.info('SIGINT received - exiting')
    try:
        umount(MOUNT_DIR)
    except ErrorReturnCode_32:
        logger.info('Folder to umount is not mounted')
    sys.exit(0)


# get files matching defined extensions using iglob
def get_file_list_iglob(mountdir: str) -> Iterator[str]:
    logger.debug('getting files for directory %s', mountdir)
    files = glob.iglob(mountdir + '/**/*.jpg', recursive=True)
    return files


# get files matching defined extensions using os.walk
# should perform best
def get_file_list_walk(path: str) -> List[str]:
    logger.debug("get_file_list_walk for %s", path)
    results = []
    for root, dirs, files in os.walk(path):
        for file in files:
            if file.endswith(FILE_EXTENSIONS):
                results.append(os.path.join(root, file))
    return results


# unmount usb and set led back to normal
def umount(path: str):
    logger.debug("umount %s", path)
    sudo.umount(path)
    led.default_trigger()


def main():
    led.default_trigger()
    signal.signal(signal.SIGINT, signal_handler)
    try:
        logger.info('main')
        context = pyudev.Context()
        monitor_block = pyudev.Monitor.from_netlink(context)
        monitor_block.filter_by(subsystem='block', device_type='partition')
        monitor_usb = pyudev.Monitor.from_netlink(context)
        # additional to filter Enumerator could be used to only iterate on devices wiht speciifc attributes
        # https://pyudev.readthedocs.io/en/v0.13/api/context.html#pyudev.Enumerator
        monitor_usb.filter_by(subsystem='usb', device_type='usb_device')
        # Wait for incoming events and receive them upon arrival.
        for device in iter(monitor_usb.poll, None):
            logger.info('First usb_device event: time %s', time.perf_counter())
            action = device.action
            if action == "add":
                logger.info('Action: %s', action)
                logger.debug('Device type: %s', device.device_type)
                logger.debug('Subsystem: %s', device.subsystem)
                logger.debug('Vendor: %s', device.get('ID_VENDOR_FROM_DATABASE'))
                logger.debug('Model: %s', device.get('ID_MODEL'))
                logger.debug('Model from database: %s', device.get('ID_MODEL_FROM_DATABASE'))

                if is_camera(device) or is_ptp(device):
                    logger.info("camera/PTP device found")
                    handle_camera_ptp(device)
                else:
                    poll_block_device_add(monitor_block)
                os.sync()

            if action == 'remove':
                logger.info('unmounting...')
                # should differentiate between block device and ptp
                umount(MOUNT_DIR)
                # clear monitor events by creating new one? how to close old one?
                # monitor_block = pyudev.Monitor.from_netlink(context)
                # monitor_block.filter_by(subsystem='block', device_type='partition')

    except Exception as ex:
        logger.exception("Exception %s", ex)
        umount(MOUNT_DIR)


def is_camera(device: Device) -> bool:
    colord_kind = device.get("COLORD_KIND")
    logger.debug("COLORD_KIND: %s", colord_kind)
    return colord_kind == "camera"


def is_ptp(device: Device) -> bool:
    id_gphoto2: int = device.get("ID_GPHOTO2")
    gphoto2_driver = device.get("GPHOTO2_DRIVER")
    logger.debug("ID_GPHOTO2: %s", id_gphoto2)
    logger.debug("GPHOTO2_DRIVER: %s", gphoto2_driver)
    return id_gphoto2 == 1 or gphoto2_driver == "PTP"


def poll_block_device_add(monitor_block: Monitor):
    for device_block in iter(partial(monitor_block.poll, 3), None):
        # use enumerator instead?
        # https://pyudev.readthedocs.io/en/v0.13/api/context.html#pyudev.Enumerator
        if device_block.action == 'add':
            logger.info('block device found')
            logger.debug('device node: %s', device_block.device_node)
            logger.debug('serial: %s', device_block.get("ID_SERIAL"))
            handle_block_device(device_block)


def handle_camera_ptp(device: Device):
    logger.info('handle_camera_ptp: time %s', time.perf_counter())
    led.on()
    logger.info('mounting ptp...')
    mkdir('-p', PTP_MOUNT_DIR)
    # -o allow_other is needed because otherwise only root can access the camera
    # https://wiki.ubuntuusers.de/Kamera_via_PTP_einbinden/#Mount-Umount-Skript-anlegen
    sudo.gphotofs(PTP_MOUNT_DIR, '-o', 'allow_other')  # works
    # but normal user is not allowed to use allow_other if not enabled in fuse.conf
    # msg = gphotofs(PTP_MOUNT_DIR, '-o', 'allow_other') #fusermount: option allow_other only allowed if 'user_allow_other' is set in /etc/fuse.conf
    logger.info('gphotofs mounted to %s', PTP_MOUNT_DIR)
    files = get_file_list_walk(PTP_MOUNT_DIR)
    encryptor.encrypt_files(files)
    led.default_trigger()
    logger.info('Finished: time %s', time.perf_counter())


def handle_block_device(device: Device):
    logger.info('handle_block_device: time %s', time.perf_counter())
    led.on()
    logger.info('mounting...')
    sudo.mkdir('-p', MOUNT_DIR)  # -p so there is no error if folder already exists
    # attention when using command via sh library:
    # the following will give "mount: unsupported option format"
    # sudo.mount('-o uid=1000,gid=1000,umask=0000', device.device_node, MOUNT_DIR)
    # whereas the next line will work --> split option and parameter of this options into two strings!
    sudo.mount('-o', 'uid=1000,gid=1000', device.device_node, MOUNT_DIR)  # mount the device /dev/sda1
    # could do an entry in /etc/fstab with user so no sudo is needed to mount
    files = get_file_list_walk(MOUNT_DIR)
    encryptor.encrypt_files(files)
    led.default_trigger()
    logger.info('Finished: time %s', time.perf_counter())


if __name__ == '__main__':
    main()
