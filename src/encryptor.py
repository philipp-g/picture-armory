#!/usr/bin/python3
import functools
import logging
import os
import timeit
from abc import ABC, abstractmethod
from itertools import islice, chain
from typing import Iterable, AnyStr, Tuple

# noinspection PyPackageRequirements
from Crypto.Cipher import AES, PKCS1_OAEP
# https://youtrack.jetbrains.com/issue/PY-23335
# https://stackoverflow.com/questions/29025468/pycharm-not-properly-recognizing-requirements-python-django
# noinspection PyPackageRequirements
from Crypto.PublicKey import RSA
# noinspection PyPackageRequirements
from Crypto.Random import get_random_bytes  # same us os.urandom
from Crypto.Util.Padding import pad
from nacl.public import PrivateKey, SealedBox, PublicKey
from profilehooks import profile

import log
from af_alg import af_alg_sendfile
from file_writer import FileWriter, DryFileWriter, RandomFolderFileWriter
from shredder import AbstractShredder, DryShredder, FileShredder

logger = logging.getLogger('root')


class AbstractEncryptor(ABC):

    @abstractmethod
    def __init__(self, public_key, private_key, file_writer: FileWriter, shredder: AbstractShredder):
        self.shredder = shredder
        self.public_key = public_key
        self.private_key = private_key
        self.file_writer = file_writer

    @abstractmethod
    def generate_keys(self):
        pass

    @abstractmethod
    def encrypt_data(self, data):
        pass

    def encrypt_file(self, file: AnyStr):
        logger.debug('encrypt_file')
        with open(file, 'rb') as file_in:
            data = file_in.read()
            data_tuple = self.encrypt_data(data)
            self.file_writer.write_tuple(data_tuple, os.path.basename(file) + ".encrypted")

    # @profile(filename="../resources/profiler_results/t480s/profile_cryptodome_encrypt_files.stats")
    def encrypt_files(self, files: Iterable[AnyStr]):
        logger.info('encrypt_files, count: %d', len(files))
        self.file_writer.target_folder = RandomFolderFileWriter.random_folder_name()
        for file in files:
            self.encrypt_file(file)
            self.shredder.shred(file)

    def decrypt_files(self, input_output_pair: Iterable[Tuple[AnyStr, AnyStr]]):
        for (infile, outfile) in input_output_pair:
            self.decrypt_file(infile, outfile)

    @abstractmethod
    def decrypt_file(self, file, target):
        pass

    @abstractmethod
    def decrypt_data(self, data):
        pass

    @staticmethod
    def split_data(data, length_list):
        i = iter(data)
        return [bytes(islice(i, n)) for n in chain(length_list, [None])]


class RSAEncryptor(AbstractEncryptor):
    RSA_KEY_LENGTH = 2048
    AES_KEY_BYTES = 32  # 16,24 or 32
    NONCE_LENGTH = 16  # GCM is most commonly used with 96-bit (12-byte) nonces but PyCryptodome uses 16 byte per default
    TAG_LENGTH = 16  # 4 to 16 bytes

    def __init__(self, public_key, file_writer: FileWriter, private_key=None):
        super().__init__(public_key, private_key, file_writer, DryShredder())
        if not os.path.isfile(public_key):
            self.generate_keys()

        with open(self.public_key) as f:
            recipient_key = RSA.import_key(f.read())
            self.cipher_rsa = PKCS1_OAEP.new(recipient_key)

        if private_key is not None:
            rsa_key = RSA.import_key(open(self.private_key).read())
            self.rsa_key_size_in_bytes = rsa_key.size_in_bytes()
            self.cipher_rsa_private = PKCS1_OAEP.new(rsa_key)

    def generate_keys(self):
        logger.info('generate_keys')
        key = RSA.generate(self.RSA_KEY_LENGTH)
        with open(self.private_key, "wb") as file_out:
            rsa_key_private = key.export_key()  # parameter passphrase=
            file_out.write(rsa_key_private)

        with open(self.public_key, "wb") as file_out:
            rsa_key_public = key.publickey().export_key()
            file_out.write(rsa_key_public)

    def encrypt_data(self, data):
        logger.debug('encrypt_data')

        session_key = get_random_bytes(self.AES_KEY_BYTES)

        # Encrypt the session key with the public RSA key
        enc_session_key = self.cipher_rsa.encrypt(session_key)

        # create aes cipher
        # EAX needs two passes per block so it is slower than other authenticated encryption modes
        # need to create new cipher everytime as it can't be reused as soon as encrpyt_and_digest was called
        # see state machine https://pycryptodome.readthedocs.io/en/latest/src/cipher/modern.html
        cipher_aes = AES.new(session_key, AES.MODE_GCM)  # if no nonce is given get_random_bytes(16) is used
        # cipher_aes=ChaCha20_Poly1305.new(key=session_key)

        # Encrypt the data with the AES cipher
        ciphertext, tag = cipher_aes.encrypt_and_digest(data)

        return enc_session_key, cipher_aes.nonce, tag, ciphertext

        # for cbc
        # cipher_aes=AES.new(session_key, AES.MODE_CBC)
        # ciphertext = cipher_aes.encrypt(pad(data,cipher_aes.block_size))
        # return enc_session_key, cipher_aes.IV, ciphertext

    def decrypt_file(self, file, target):
        logger.info('decrypt')
        file_in = open(file, "rb")

        enc_session_key, nonce, tag, ciphertext = \
            (file_in.read(e) for e in (self.rsa_key_size_in_bytes, self.NONCE_LENGTH, self.TAG_LENGTH, -1))

        data = self.decrypt(enc_session_key, nonce, tag, ciphertext)

        with open(target, "wb") as file_out:
            file_out.write(data)

    def decrypt_data(self, data):
        data_parts = self.split_data(data, [self.rsa_key_size_in_bytes, self.NONCE_LENGTH, self.TAG_LENGTH])
        enc_session_key = data_parts[0]
        nonce = data_parts[1]
        tag = data_parts[2]
        ciphertext = data_parts[3]
        return self.decrypt(enc_session_key, nonce, tag, ciphertext)

    def decrypt(self, enc_session_key, nonce, tag, ciphertext):
        # Decrypt the session key with the private RSA key
        session_key = self.cipher_rsa_private.decrypt(enc_session_key)

        # Decrypt the data with the AES session key
        cipher_aes = AES.new(session_key, AES.MODE_GCM, nonce)
        return cipher_aes.decrypt_and_verify(ciphertext, tag)


class PyNaClEncryptor(AbstractEncryptor):

    def __init__(self, public_key, file_writer: FileWriter, private_key=None):
        super().__init__(public_key, private_key, file_writer, FileShredder())
        if not os.path.exists(public_key):
            self.generate_keys()
        pk = PublicKey(open(self.public_key, "rb").read())
        self.sealed_box = SealedBox(pk)

        if private_key is not None:
            sk = PrivateKey(open(self.private_key, "rb").read())
            self.unseal_box = SealedBox(sk)

    def generate_keys(self):
        sk = PrivateKey.generate()
        pk = sk.public_key
        with open(self.private_key, "wb") as file_out:
            file_out.write(bytes(sk))
        with open(self.public_key, "wb") as file_out:
            file_out.write(bytes(pk))

    def encrypt_data(self, data):
        encrypted = self.sealed_box.encrypt(
            data)  # as pynacl uses chacha20 where encryption=decrypiton operation it shoud be similar fast
        return encrypted,  # return tuple with one element, "," is needed to make it a tuple
        # alternatively use list: [encrypted]

    def decrypt_file(self, file, target):
        with open(file, 'rb') as file_in:
            data = self.unseal_box.decrypt(file_in.read())
            with open(target, "wb") as file_out:
                file_out.write(data)

    def decrypt_data(self, data):
        return self.unseal_box.decrypt(data)


class AfAlgEncryptor(RSAEncryptor):
    IV_LENGTH = 16
    KEY_LENGTH = 32

    def __init__(self, public_key, file_writer: FileWriter, private_key=None):
        super().__init__(public_key, file_writer, private_key)

    def encrypt_data(self, data):
        iv = os.urandom(self.IV_LENGTH)
        session_key = os.urandom(self.KEY_LENGTH)
        ciphertext = af_alg_sendfile.encrypt_data(session_key, iv, data)
        enc_session_key = self.cipher_rsa.encrypt(session_key)
        data_tuple = (enc_session_key, iv, ciphertext)
        return data_tuple

    def decrypt_data(self, data):
        data_parts = self.split_data(data, [self.rsa_key_size_in_bytes, self.IV_LENGTH])
        enc_session_key = data_parts[0]
        session_key = self.cipher_rsa_private.decrypt(enc_session_key)
        iv = data_parts[1]
        ciphertext = data_parts[2]
        return af_alg_sendfile.decrypt_data(session_key, iv, ciphertext)

    def decrypt_file(self, file, target):
        logger.info('decrypt')
        with open(file, "rb") as file_in:
            # private_key = RSA.import_key(open(self.private_key).read())
            # output of rsa encryption is always equal to key modulus
            # therefore enc_session_key has size: private_key.size_in_bytes()
            enc_session_key, iv, ciphertext = \
                (file_in.read(e) for e in (self.rsa_key_size_in_bytes, self.IV_LENGTH, -1))

            plaintext = self._decrypt_sendfile(enc_session_key, iv, file_in)

            with open(target, "wb") as file_out:
                file_out.write(plaintext)

    def _decrypt_sendfile(self, enc_session_key, iv, file_in):
        # Decrypt the session key with the private RSA key
        session_key = self.cipher_rsa_private.decrypt(enc_session_key)
        plaintext = af_alg_sendfile.decrypt(session_key, iv, file_in,
                                            offset=self.rsa_key_size_in_bytes + self.IV_LENGTH)
        return plaintext

    def encrypt_file(self, file: AnyStr):
        logger.debug('encrypt_file %s', file)
        iv = os.urandom(self.IV_LENGTH)
        session_key = os.urandom(self.KEY_LENGTH)
        with open(file, "rb") as file_in:
            ciphertext = af_alg_sendfile.encrypt(session_key, iv, file_in)
        enc_session_key = self.cipher_rsa.encrypt(session_key)
        # write to file: enc_session_key, iv, ciphertext
        data_tuple = (enc_session_key, iv, ciphertext)
        self.file_writer.write_tuple(data_tuple, os.path.basename(file) + ".encrypted")


def time_encrypt_data(y, data_list):
    for data in data_list:
        y.encrypt_data(data)


# @profile(filename="../resources/profiler_results/profile_afalg.stats")
def time_encrypt_decrypt(y: AbstractEncryptor, encrypt_files, decrypt_file_pairs):
    y.encrypt_files(encrypt_files)
    y.decrypt_files(decrypt_file_pairs)


if __name__ == '__main__':
    # Purpose of timeit is to get the throughput of a function, which will always require the code to run multiple times to fade out edge cases and give a good average.
    #
    # While cProfile, on the other hand, is used to profile each sub-call of the function's stack, to demystify all the magic happening inside a function.
    #
    # timeit will tell you that there's some optimisation required to function,
    # while cProfile will point you to the right direction telling you which minuscule part of stack is hogging up your turnaround time.
    log.setup_logger('root')
    logger.info('encryptor main')
    logger.setLevel(logging.WARNING)
    # x = PyNaClEncryptor("public_pynacl.pem", "private_pynacl.pem")
    # x = AfAlgEncryptor("public.pem", "private.pem", "/home/philipp//PycharmProjects/usbarmory/resources/images")
    # x.encrypt_files(['../resources/images/pic1.jpg', '../resources/images/pic2.jpg'])

    # y = RSAEncryptor("public.pem", FileWriter('../resources/images/', 'out'), "private.pem")
    # y = RSAEncryptor("public.pem", DryFileWriter(), "private.pem")
    y = PyNaClEncryptor("public_pynacl.pem", FileWriter('../resources/images/', 'out'), "private_pynacl.pem")
    # y = PyNaClEncryptor("public_pynacl.pem", RandomFolderFileWriter('../resources/images/'), "private_pynacl.pem")
    # y = PyNaClEncryptor("public_pynacl.pem", DryFileWriter(), "private_pynacl.pem")
    # y = AfAlgEncryptor("public.pem", FileWriter('../resources/images/', 'out'), "private.pem")
    # y = AfAlgEncryptor("public.pem", RandomFolderFileWriter('../resources/images/'), "private.pem")
    # y = AfAlgEncryptor("public.pem", DryFileWriter(), "private.pem")

    # check if RSAEncryptor decrypt_data works
    # y = RSAEncryptor("public.pem", FileWriter('../resources/images/', 'out'), "private.pem")
    # y.encrypt_file('../resources/images/pic2.jpg')
    # with open('../resources/images/out/pic2.jpg.encrypted','rb') as file:
    #     encrypted_data=file.read()
    # with open('../resources/images/pic2_decrypted_test.jpg', 'wb') as out:
    #     out.write(y.decrypt_data(encrypted_data))

    # check if AfAlg decrypt_data works
    # y = AfAlgEncryptor("public.pem", FileWriter('../resources/images/', 'out'), "private.pem")
    # y.encrypt_file('../resources/images/pic2.jpg')
    # with open('../resources/images/out/pic2.jpg.encrypted','rb') as file:
    #     encrypted_data=file.read()
    # with open('../resources/images/pic2_decrypted_test.jpg', 'wb') as out:
    #     out.write(y.decrypt_data(encrypted_data))

    # check if PyNacl decrypt_data works
    # y = PyNaClEncryptor("public_pynacl.pem", FileWriter('../resources/images/', 'out'), "private_pynacl.pem")
    # print(b"test"==y.decrypt_data(y.encrypt_data(b"test")[0]))

    # check if AfAlg encrypt_data works
    # y = AfAlgEncryptor("public.pem", FileWriter('../resources/images/', 'out'), "private.pem")
    # y.encrypt_file('../resources/images/pic2.jpg')
    # with open('../resources/images/pic2.jpg', 'rb') as input_file:
    #     data=input_file.read()
    #     x= FileWriter('../resources/images/', 'out')
    #     x.write_tuple(y.encrypt_data(data), os.path.basename('../resources/images/pic2.jpg') + ".encrypted2")
    #
    # y.decrypt_file('../resources/images/out/pic2.jpg.encrypted2','../resources/images/pic2_decrypted.jpg')

    print(type(y).__name__)
    file_list = ['../resources/images/eos1.jpg',
                 '../resources/images/eos2.jpg',
                 '../resources/images/eos3.jpg',
                 '../resources/images/eos4.jpg',
                 '../resources/images/eos5.jpg',
                 '../resources/images/eos6.jpg',
                 '../resources/images/eos7.jpg',
                 '../resources/images/eos8.jpg',
                 '../resources/images/eos9.jpg',
                 '../resources/images/eos10.jpg'
                 ]

    #read + encrypt files
    print("timeit: read + encrypt")

    data = timeit.repeat(
        functools.partial(y.encrypt_files, file_list),
        number=1,
        repeat=10)

    # encrypt only
    # data_list = []
    # for file in file_list:
    #     with open(file, 'rb') as file_in:
    #         data_list.append(file_in.read())
    # print("timeit: encrypt only")
    #
    # data = timeit.repeat(
    #     functools.partial(time_encrypt_data, y, data_list),
    #     number=1,
    #     repeat=40
    # )

    print(data)
    print(min(data))

    # timeit functools approach from https://stackoverflow.com/a/32556672/6151869
    # read + decrypt files
    # print("timeit: read + decrypt")
    #
    # decrypt_file_pairs = [('../resources/images/out/eos1.jpg.encrypted', '../resources/images/eos1_decrypted.jpg'),
    #                       ('../resources/images/out/eos2.jpg.encrypted', '../resources/images/eos2_decrypted.jpg'),
    #                       ('../resources/images/out/eos3.jpg.encrypted', '../resources/images/eos3_decrypted.jpg'),
    #                       ('../resources/images/out/eos4.jpg.encrypted', '../resources/images/eos4_decrypted.jpg'),
    #                       ('../resources/images/out/eos5.jpg.encrypted', '../resources/images/eos5_decrypted.jpg'),
    #                       ('../resources/images/out/eos6.jpg.encrypted', '../resources/images/eos6_decrypted.jpg'),
    #                       ('../resources/images/out/eos7.jpg.encrypted', '../resources/images/eos7_decrypted.jpg'),
    #                       ('../resources/images/out/eos8.jpg.encrypted', '../resources/images/eos8_decrypted.jpg'),
    #                       ('../resources/images/out/eos9.jpg.encrypted', '../resources/images/eos9_decrypted.jpg'),
    #                       ('../resources/images/out/eos10.jpg.encrypted', '../resources/images/eos10_decrypted.jpg'),
    #                       ]
    #
    # print(
    #     timeit.repeat(
    #         functools.partial(y.decrypt_files, decrypt_file_pairs),
    #         number=1,
    #         repeat=5))

    # read + encrypt + decrypt + write
    # print("timeit: read + encrypt + decrypt + write")
    # print(
    #     timeit.repeat(
    #         functools.partial(time_encrypt_decrypt, y, file_list, decrypt_file_pairs),
    #         number=1,
    #         repeat=5)
    # )

    # profile
    # time_encrypt_decrypt(y, file_list, decrypt_file_pairs)
    #y.encrypt_files(file_list)
