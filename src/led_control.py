import logging
from enum import Enum
from typing import Union

from sh import sudo, echo

logger = logging.getLogger('root')


class LedMode(Enum):
    HEARTBEAT = 'heartbeat'
    CPU = 'cpu'
    DISK = 'mmc0'
    NONE = 'none'
    DEFAULT = HEARTBEAT


def trigger_off():
    # no trigger = led off
    set_trigger_mode(LedMode.NONE)


def default_trigger():
    logger.debug("set LED trigger to default")
    set_trigger_mode(LedMode.DEFAULT)


def set_trigger_mode(mode: LedMode):
    logger.debug("set LedMode to %s", mode)
    sudo_echo_to(mode.value, "/sys/class/leds/LED/trigger")


def off():
    logger.debug("LED off")
    set_brightness(0)  # also sets trigger to none
    # trigger_off not really necessary
    trigger_off()


def on():
    logger.debug("LED on")
    trigger_off()  # we need trigger off so the led is on permanently
    set_brightness(1)


# in case of usbarmory there is only on(>0) or off(0) and no difference between brightness 1 and 255
def set_brightness(brightness: int):
    logger.debug("set LED brightness to %d", brightness)
    sudo_echo_to(brightness, "/sys/class/leds/LED/brightness")


# using tee to echo to some file with sudo rights
def sudo_echo_to(value: Union[str, int], target: str):
    sudo.tee(echo(value), target)
