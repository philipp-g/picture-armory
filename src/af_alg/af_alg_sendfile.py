# https://github.com/crazyguitar/pysheeet/blob/master/docs/notes/python-socket.rst#aes-gcm-encrypt-decrypt-file-with-sendfile
# need python 3.6 or above & Linux >=4.9
import contextlib
import os
import socket
import threading

# blocksize of aes(cbc), needed for padding
BS = 16  # Bytes
CHUNK_SIZE = 4096  # Bytes


# PKCS#7 padding but at beginning of file
def padding(filesize: int):
    return (BS - filesize % BS) * \
           chr(BS - filesize % BS).encode('utf-8')


def upad(data):
    return data[data[0]:]


# https://docs.python.org/3/library/contextlib.html#contextlib.contextmanager
@contextlib.contextmanager
def create_alg(typ, name):
    # The SOCK_SEQPACKET socket type is similar to the SOCK_STREAM type, and is also connection-oriented.
    # The only difference between these types is that record boundaries are maintained using the SOCK_SEQPACKET type.
    # A record can be sent using one or more output operations and received using one or more input operations,
    # but a single operation never transfers parts of more than one record.
    # Record boundaries are visible to the receiver via the MSG_EOR flag in the received message flags returned by the recvmsg() function.
    # https://stackoverflow.com/questions/10104082/unix-socket-sock-seqpacket-vs-sock-dgram
    s = socket.socket(socket.AF_ALG, socket.SOCK_SEQPACKET)
    try:
        s.bind((typ, name))
        yield s
    finally:
        s.close()


def do_sendfile(fout, fin, count, fin_len):
    l = fin_len
    offset = 0
    while l > 0:
        ret = fout.sendfile(fin, offset, count)
        offset += ret
        l -= ret


def do_recv(fout, fin):
    while True:
        data = fin.recv(CHUNK_SIZE)
        if not data:
            break

        fout.write(data)


def do_recv_buf(fin):
    all_data = bytearray()
    while True:
        data = fin.recv(CHUNK_SIZE)
        if not data:
            break
        all_data.extend(data)
    return all_data


def do_recv_buf_size(fin, size):
    all_data = bytearray()

    while size > 0:
        data = fin.recv(CHUNK_SIZE)  # sometimes get len(data)=0 when starting to receive file
        # https://stackoverflow.com/questions/38021659/can-a-c-socket-recv-0-bytes-without-the-client-shutting-the-connection
        # The first point is incorrect in the case of a SOCK_DGRAM or SOCK_SEQPACKET socket, which CAN have 0-length messages.
        # print("received %d bytes", len(data))
        size -= len(data)
        all_data.extend(data)
    # print("finished recv")
    return all_data


def encrypt_data(key, iv, data):
    len(data)
    with create_alg('skcipher', 'cbc(aes)') as algo:
        algo.setsockopt(socket.SOL_ALG,
                        socket.ALG_SET_KEY, key)
        op, _ = algo.accept()
        op.sendmsg_afalg(op=socket.ALG_OP_ENCRYPT,
                         iv=iv,
                         flags=socket.MSG_MORE)
        padding_data = padding(len(data))
        threading.Thread(target=op.sendall, args=(padding_data + data,)).start()
        ciphertext = do_recv_buf_size(op, len(padding_data) + len(data))
    return ciphertext


def decrypt_data(key, iv, ciphertext):
    with create_alg('skcipher', 'cbc(aes)') as algo:
        algo.setsockopt(socket.SOL_ALG, socket.ALG_SET_KEY, key)
        op, _ = algo.accept()
        op.sendmsg_afalg(op=socket.ALG_OP_DECRYPT,
                         iv=iv,
                         flags=socket.MSG_MORE)
        threading.Thread(target=op.sendall, args=(ciphertext,)).start()

        plaintext = do_recv_buf_size(op, len(ciphertext))

    return upad(plaintext)


def encrypt(key, iv, pfile):
    pfd = pfile.fileno()
    st = os.fstat(pfd)
    totalbytes = st.st_size
    with create_alg('skcipher', 'cbc(aes)') as algo:
        algo.setsockopt(socket.SOL_ALG,
                        socket.ALG_SET_KEY, key)
        # should probably also setsockopt iv with socket.ALG_SET_IV
        # algo.setsockopt(socket.SOL_ALG, socket.ALG_SET_IV, iv)
        op, _ = algo.accept()

        with op:
            op.sendmsg_afalg(op=socket.ALG_OP_ENCRYPT,
                             iv=iv,
                             flags=socket.MSG_MORE)
            # MSG_MORE: If this flag is set, the send system call acts like a cipher update function
            # where more input data is expected with a subsequent invocation of the send system call.

            # pad in the beginning as there is no possibility so set MSG_MORE for sendfile
            padding_data = padding(totalbytes)
            op.sendall(padding_data, socket.MSG_MORE)

            # using sendfile to encrypt file data
            # two possibilities
            # os.sendfile(op.fileno(), pfd, offset, totalbytes)
            # op.sendfile(pfile)

            # Do sending in another thread as we need to read result to free buffer
            # https://unix.stackexchange.com/questions/38043/size-of-data-that-can-be-written-to-read-from-sockets
            # $ sysctl net.core.rmem_max
            # net.core.rmem_max = 212992
            # same for wmem_max
            # but 209602 still does not work, 208xxx seems to work, e.g. 208890 bytes
            threading.Thread(target=op.sendfile, args=(pfile,)).start()
            # alternative: using do_sendfile to send in self defined chunks
            # threading.Thread(target=do_sendfile, args=(op,pfile,CHUNK_SIZE,totalbytes)).start()

            # Result:
            # directly write to file
            # with open("output", "wb") as f:
            # do_recv(f, op)

            # Tried to use sendfile to write output but
            # The in_fd argument must correspond to a file which supports mmap(2)-like operations (i.e., it cannot be a socket).
            # os.sendfile(f.fileno(), op.fileno(), offset, totalbytes+len(padding_data))

            # Receive file into buffer then later write
            ciphertext = do_recv_buf_size(op, len(padding_data) + totalbytes)

    return ciphertext


def decrypt(key, iv, in_file, offset=0):

    with create_alg('skcipher', 'cbc(aes)') as algo:
        algo.setsockopt(socket.SOL_ALG,
                        socket.ALG_SET_KEY, key)
        op, _ = algo.accept()
        with op:
            op.sendmsg_afalg(op=socket.ALG_OP_DECRYPT,
                             iv=iv,
                             flags=socket.MSG_MORE)
            # https://stackoverflow.com/questions/25734595/python-threading-confusing-code-int-object-is-not-callable
            totalbytes = os.path.getsize(in_file.name)
            # if using offset also need to give size to read or recv will block
            threading.Thread(target=op.sendfile, args=(in_file, offset, totalbytes - offset)).start()
            # threading.Thread(target=os.sendfile, args=(op.fileno(),in_file.fileno(),offset,totalbytes-offset)).start()
            # On Linux, if offset is given as None, the bytes are read from the current position of in and the position of in is updated.
            # So if we read the data we want to skip before offset=None should also work

            plaintext = do_recv_buf(op)

    return upad(plaintext)
